package agenda.m3m6.chaparro.inspedralbes.cat.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import agenda.m3m6.chaparro.inspedralbes.cat.entities.Contacte;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Feina;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Personal;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Telefon;


public class AgendaHSQLDBManager implements AgendaInterfaceManager {
	private static final String URLHSQL = "jdbc:hsqldb:file:C:/Users/Axell/Desktop/Instituto/Segundo/AgendaDB";
	//private static final String URLHSQL = "jdbc:hsqldb:file:/home/ausias/Escriptori/dades/dades/Axell_Chaparro(2020)/Eclipe EE/AgendaDB";
	private static final String PASSHSQL = "";
	private static final String USUARIHSQL = "SA";
	private static final String TABLE_CONTACTES = "CONTACTE";
	private static final String TABLE_PERSONAL = "PERSONAL";
	private static final String TABLE_FEINA = "FEINA";
	private static final String COLUMNA_NIF = "NIF";
	private static final String COLUMNA_NOM = "NOM";
	private static final String COLUMNA_ADRECA = "ADRECA";
	private static final String TABLE_GRUP = "GRUP";
	private static final String TABLE_GRU_CON = "GRU_CON";
	private static final String COLUMNA_GRUP_CODI = "CODI";
	private static final String COLUMNA_GRU_CON_CODI = "CODI_GRUP";
	private static final String COLUMNA_GRU_CON_NIF = "NIF_CONTACTE_GRUP";
	private static final String COLUMNA_GRUP_NOM = "NOM";
	private static final String COLUMNA_IMATGE = "IMATGE";
	private static final String COLUMNA_WEB = "WEB";
	private static final String TABLE_TELEFON = "TELEFON";
	private static final String COLUMNA_TELEFON_NUMERO = "NUMERO";
	private static final String COLUMNA_TELEFON_NIF_CONTACTE = "NIF_CONTACTE";

	public Connection connection;

	public void openConnection() throws SQLException {
		connection = DriverManager.getConnection(URLHSQL, USUARIHSQL, PASSHSQL);
	}

	public void closeConnection() throws SQLException {
		connection.close();
	}

	public void createDataBaseWithSamples() {

		Statement statement;
		try {
			openConnection();
			statement = connection.createStatement();
			statement.executeUpdate("DROP TABLE IF EXISTS CONTACTE");
			statement.executeUpdate(
					"CREATE TABLE CONTACTE (NIF VARCHAR(50) PRIMARY KEY, NOM VARCHAR(50), ADRECA VARCHAR(50));");
			statement.executeUpdate("INSERT INTO CONTACTE VALUES('123A','Axell-Feina', 'Calle callejera 1')");
			statement.executeUpdate("INSERT INTO CONTACTE VALUES('456B','Axell-Personal', 'Calle callejera 2')");

			statement.executeUpdate("DROP TABLE IF EXISTS FEINA");
			statement.executeUpdate(
					"CREATE TABLE FEINA (NIF VARCHAR(50) PRIMARY KEY, NOM VARCHAR(50), ADRECA VARCHAR(50), WEB VARCHAR(200));");
			statement.executeUpdate(
					"INSERT INTO FEINA VALUES('123A','Axell-Feina', 'Calle callejera 1', 'www.linkedin/axellChaparro.com')");

			statement.executeUpdate("DROP TABLE IF EXISTS GRUP");
			statement.executeUpdate("CREATE TABLE GRUP (CODI INTEGER PRIMARY KEY, NOM VARCHAR(50));");
			statement.executeUpdate("INSERT INTO GRUP VALUES(1,'Grup-1')");
			statement.executeUpdate("INSERT INTO GRUP VALUES(2,'Grup-2')");
			statement.executeUpdate("INSERT INTO GRUP VALUES(3,'Grup-3')");

			statement.executeUpdate("DROP TABLE IF EXISTS GRU_CON");
			statement.executeUpdate(
					"CREATE TABLE GRU_CON (CODI_GRUP INTEGER, NIF_CONTACTE_GRUP VARCHAR(50),  PRIMARY KEY (CODI_GRUP, NIF_CONTACTE_GRUP)  );");
			statement.executeUpdate("INSERT INTO GRU_CON VALUES(1, '123A')");
			statement.executeUpdate("INSERT INTO GRU_CON VALUES(2, '123A')");
			statement.executeUpdate("INSERT INTO GRU_CON VALUES(2, '456B')");
			statement.executeUpdate("INSERT INTO GRU_CON VALUES(3, '456B')");

			statement.executeUpdate("DROP TABLE IF EXISTS PERSONAL");
			statement.executeUpdate(
					"CREATE TABLE PERSONAL (NIF VARCHAR(50) PRIMARY KEY, NOM VARCHAR(50), ADRECA VARCHAR(50), IMATGE VARCHAR(200));");
			statement.executeUpdate(
					"INSERT INTO PERSONAL VALUES('456B','Axell-Personal', 'Calle callejera 2', 'www.instagram/axell.com')");

			statement.executeUpdate("DROP TABLE IF EXISTS TELEFON");
			statement.executeUpdate("CREATE TABLE TELEFON (NUMERO BIGINT PRIMARY KEY, NIF_CONTACTE VARCHAR(50));");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(100000000,'123A')");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(200000000,'123A')");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(300000000,'123A')");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(400000000,'456B')");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(500000000,'456B')");
			statement.executeUpdate("INSERT INTO TELEFON VALUES(600000000,'456B')");

			statement.close();
			closeConnection();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void afegirContacte(Contacte contacte) {
		try {
			openConnection();
			String queryInsert = "INSERT INTO " + TABLE_CONTACTES + " VALUES (?,?,?)";
			try (PreparedStatement statement = connection.prepareStatement(queryInsert)) {
				statement.setString(1, contacte.getNif());
				statement.setString(2, contacte.getNom());
				statement.setString(3, contacte.getAdreca());
				statement.executeUpdate();
			}

			if (contacte instanceof Personal) {
				queryInsert = "INSERT INTO " + TABLE_PERSONAL + " VALUES (?,?,?,?)";
				try (PreparedStatement statement = connection.prepareStatement(queryInsert)) {
					statement.setString(1, contacte.getNif());
					statement.setString(2, contacte.getNom());
					statement.setString(3, contacte.getAdreca());
					statement.setString(4, ((Personal) contacte).getImatge());
					statement.executeUpdate();
				}
			} else if (contacte instanceof Feina) {
				queryInsert = "INSERT INTO " + TABLE_FEINA + " VALUES (?,?,?,?)";
				try (PreparedStatement statement = connection.prepareStatement(queryInsert)) {
					statement.setString(1, contacte.getNif());
					statement.setString(2, contacte.getNom());
					statement.setString(3, contacte.getAdreca());
					statement.setString(4, ((Feina) contacte).getWeb());
					statement.executeUpdate();
				}
			}

			closeConnection();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void eliminarContacte(Contacte contacte) {

		try {
			openConnection();

			String query = "DELETE FROM " + TABLE_CONTACTES + " WHERE " + COLUMNA_NIF + " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, contacte.getNif());
				statement.executeUpdate();
			}

			query = "DELETE FROM " + TABLE_PERSONAL + " WHERE " + COLUMNA_NIF + " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, contacte.getNif());

				statement.executeUpdate();
			}

			query = "DELETE FROM " + TABLE_FEINA + " WHERE " + COLUMNA_NIF + " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, contacte.getNif());
				statement.executeUpdate();
			}
			
			query = "DELETE FROM " + TABLE_TELEFON + " WHERE " + COLUMNA_TELEFON_NIF_CONTACTE + " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, contacte.getNif());
				statement.executeUpdate();
			}
			
			query = "DELETE FROM " + TABLE_GRU_CON + " WHERE " + COLUMNA_GRU_CON_NIF + " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setString(1, contacte.getNif());
				statement.executeUpdate();
			}

			closeConnection();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void actualitzarContacte(Contacte contacteOriginal, Contacte contacteActualitzat) {
		
		String newNom = contacteActualitzat.getNom();
		String newAdreca = contacteActualitzat.getAdreca();
		String newWeb = null;
		String newImatge= null;
		if(contacteActualitzat instanceof Feina) {
			newWeb = ((Feina) contacteActualitzat).getWeb();
		}else {
			 newImatge= ((Personal) contacteActualitzat).getImatge();
		}
		
		if(newNom!=null&&!newNom.equals("")) {
			contacteOriginal.setNom(newNom);
		}
		if(newAdreca!=null&&!newAdreca.equals("")) {
			contacteOriginal.setAdreca(newAdreca);
		}
		if(contacteOriginal instanceof Feina) {
			
			if(newWeb!=null&&!newWeb.equals("")) {
				((Feina) contacteOriginal).setWeb(newWeb);
			}	
		}else {
			if(newImatge!=null&&!newImatge.equals("")) {
			((Personal) contacteOriginal).setImatge(newImatge);
			}
		}
		
		
		try {
			openConnection();
			String query = "UPDATE "+ TABLE_CONTACTES+ " SET "+ COLUMNA_ADRECA + " =? , "+ COLUMNA_NOM +" = ? WHERE "+ COLUMNA_NIF + " = ?";
			try(PreparedStatement statement = connection.prepareStatement(query)){
				statement.setString(1, contacteOriginal.getAdreca());
				statement.setString(2, contacteOriginal.getNom());
				statement.setString(3, contacteOriginal.getNif());
				statement.executeUpdate();
			}
			if(contacteOriginal instanceof Feina) {
				String queryFeina = "UPDATE "+ TABLE_FEINA+ " SET "+ COLUMNA_ADRECA + " =? , "+ COLUMNA_NOM +" = ?, "+ COLUMNA_WEB+ " = ? WHERE "+ COLUMNA_NIF + " = ?";
				try(PreparedStatement statement = connection.prepareStatement(queryFeina)){
					statement.setString(1, contacteOriginal.getAdreca());
					statement.setString(2, contacteOriginal.getNom());
					statement.setString(3, ((Feina) contacteOriginal).getWeb());
					statement.setString(4, contacteOriginal.getNif());
					statement.executeUpdate();
				}
			}else {
				String queryPersonal = "UPDATE "+ TABLE_PERSONAL+ " SET "+ COLUMNA_ADRECA + " =? , "+ COLUMNA_NOM +" = ?, "+ COLUMNA_IMATGE+ " = ?  WHERE "+ COLUMNA_NIF + " = ? ";
				try(PreparedStatement statement = connection.prepareStatement(queryPersonal)){
					statement.setString(1, contacteOriginal.getAdreca());
					statement.setString(2, contacteOriginal.getNom());
					statement.setString(3, ((Personal) contacteOriginal).getImatge());
					statement.setString(4, contacteOriginal.getNif());
					statement.executeUpdate();
				}
			}
	
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				

	}

	@Override
	public List<Contacte> buscarTotContactes() {
		
	ArrayList<Contacte> contactesPerNom = new ArrayList<Contacte>();
		
		try {
			openConnection();
			
			String queryNoms ="SELECT "+ COLUMNA_NIF+" FROM "+ TABLE_CONTACTES;
			PreparedStatement statement = connection.prepareStatement(queryNoms);
			
			ResultSet resultatNoms = statement.executeQuery();
			
			while(resultatNoms.next()) {
				String nif = resultatNoms.getString(COLUMNA_NIF);
			
				
				Personal personal = getInfoPersonal(nif);
				Feina feina = getInfoFeina(nif);
				if (personal != null) {
					contactesPerNom.add(personal);
				}
				if (feina != null) {
					contactesPerNom.add(feina);
				}
			}
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return contactesPerNom;
	
		
	}

	@Override
	public List<Contacte> buscarContactesPerNom(String nom) {
		
		ArrayList<Contacte> contactesPerNom = new ArrayList<Contacte>();
		
		try {
			openConnection();
			
			String queryNoms ="SELECT "+ COLUMNA_NIF+" FROM "+ TABLE_CONTACTES+ " WHERE "+ COLUMNA_NOM+ " = ?";
			PreparedStatement statement = connection.prepareStatement(queryNoms);
			statement.setString(1, nom);	
			ResultSet resultatNoms = statement.executeQuery();
			
			while(resultatNoms.next()) {
				String nif = resultatNoms.getString(COLUMNA_NIF);
				Personal personal = getInfoPersonal(nif);
				Feina feina = getInfoFeina(nif);
				if (personal != null) {
					contactesPerNom.add(personal);
				}
				if (feina != null) {
					contactesPerNom.add(feina);
				}
			}
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return contactesPerNom;
	}

	@Override
	public void crearGrup(Grup grup) {
		int maxID = 0;
		try {
			openConnection();

			String queryMaxID = "SELECT MAX(" + COLUMNA_GRUP_CODI + ") AS CODI FROM " + TABLE_GRUP;

			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(queryMaxID);

			if (result.next()) {
				maxID = result.getInt("CODI");
			}
		

			String query = "INSERT INTO " + TABLE_GRUP + " VALUES( ? , ? )";
			try (PreparedStatement prepStatement = connection.prepareStatement(query)) {
				prepStatement.setInt(1, maxID + 1);
				prepStatement.setString(2, grup.getNom());
				prepStatement.executeUpdate();
			}
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void eliminarGrup(Grup grup) {
		try {
			openConnection();

			String query = "DELETE FROM " + TABLE_GRUP + " WHERE " + COLUMNA_GRUP_CODI + " = ?";
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setInt(1, grup.getCodi());
				preparedStatement.executeUpdate();
			}

			query = "DELETE FROM " + TABLE_GRU_CON + " WHERE " + COLUMNA_GRU_CON_CODI + " = ?";
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setInt(1, grup.getCodi());
				preparedStatement.executeUpdate();
			}
			closeConnection();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	public void afegirContacteAGrup(Contacte contacte, Grup grup) {
		try {
			openConnection();

			String query = "INSERT INTO " + TABLE_GRU_CON + " VALUES ( ?, ?)";
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setInt(1, grup.getCodi());
				preparedStatement.setString(2, contacte.getNif());
				preparedStatement.executeUpdate();
			}
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void eliminarContacteDeGrup(Contacte contacte, Grup grup) {
		try {
			openConnection();

			String query = "DELETE FROM " + TABLE_GRU_CON + " WHERE " + COLUMNA_GRU_CON_CODI + "= ? AND  "
					+ COLUMNA_GRU_CON_NIF + " = ?";
			try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
				preparedStatement.setInt(1, grup.getCodi());
				preparedStatement.setString(2, contacte.getNif());
				preparedStatement.executeUpdate();
			}
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Grup veureDadesGrup(Grup grup) {
		
		Grup grupTrobat = new Grup();
		ArrayList<Contacte> contactesGrupTrobat = new ArrayList<Contacte>();

		try {
			openConnection();

			String query = "SELECT * FROM " + TABLE_GRUP + " WHERE " + COLUMNA_GRUP_CODI + " = ? ";

			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setInt(1, grup.getCodi());

				
				ResultSet result = statement.executeQuery();

				while (result.next()) {
				
				grupTrobat.setCodi(result.getInt(COLUMNA_GRUP_CODI));
				grupTrobat.setNom(result.getString(COLUMNA_GRUP_NOM));
				}
			}
			query = "SELECT " + COLUMNA_GRU_CON_NIF + " FROM " + TABLE_GRU_CON + " WHERE " + COLUMNA_GRU_CON_CODI
					+ " = ?";
			try (PreparedStatement statement = connection.prepareStatement(query)) {
				statement.setInt(1, grup.getCodi());

				ResultSet result = statement.executeQuery();
				
				while (result.next()) {
					String nif = result.getString(COLUMNA_GRU_CON_NIF);
					Personal personal = getInfoPersonal(nif);
					Feina feina = getInfoFeina(nif);
					if (personal != null) {
						contactesGrupTrobat.add(personal);
					}
					if (feina != null) {

						contactesGrupTrobat.add(feina);
					}
				}
				grupTrobat.setContactes(contactesGrupTrobat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return grupTrobat;
	}

	public Personal getInfoPersonal(String nif) throws SQLException {
		
		Personal personal = null;
		ArrayList<Telefon> telefons = new ArrayList<Telefon>();

		String queryInfoPersonal = "SELECT * FROM " + TABLE_PERSONAL + " WHERE " + COLUMNA_NIF + " = ?  ";
		try (PreparedStatement statementInfoPersonals = connection.prepareStatement(queryInfoPersonal)) {
			statementInfoPersonals.setString(1, nif);

			ResultSet resultPersonal = statementInfoPersonals.executeQuery();
			while (resultPersonal.next()) {
				personal = new Personal();
				personal.setNif(resultPersonal.getString(COLUMNA_NIF));
				personal.setAdreca(resultPersonal.getString(COLUMNA_ADRECA));
				personal.setNom(resultPersonal.getString(COLUMNA_NOM));
				personal.setImatge(resultPersonal.getString(COLUMNA_IMATGE));
			}String getTelefons = "SELECT * FROM "+ TABLE_TELEFON + " WHERE "+ COLUMNA_TELEFON_NIF_CONTACTE +"= ?";
			if(personal==null) {
				return null;
			}
			try(PreparedStatement statementTelefons = connection.prepareStatement(getTelefons)){
				statementTelefons.setString(1, nif);
				ResultSet resultats = statementTelefons.executeQuery();
				
				while(resultats.next()) {
					Telefon telefon = new Telefon();
					telefon.setNumero(resultats.getInt(COLUMNA_TELEFON_NUMERO));
					telefons.add(telefon);
				}
				personal.setTelefons(telefons);
				
			}
			String queryGrups = "SELECT * FROM "+TABLE_GRU_CON +" WHERE "+ COLUMNA_GRU_CON_NIF +" = ? ";
			try(PreparedStatement statementGrups = connection.prepareStatement(queryGrups)){
				statementGrups.setString(1, nif);
				ResultSet resultatsGrups = statementGrups.executeQuery();
				ArrayList<Grup> grups = new ArrayList<Grup>();
				while(resultatsGrups.next()) {
					String queryDadesGrup = "SELECT * FROM "+TABLE_GRUP+" WHERE "+ COLUMNA_GRUP_CODI +" = ?";
					try(PreparedStatement statementInfoGrup = connection.prepareStatement(queryDadesGrup)){
						statementInfoGrup.setString(1, resultatsGrups.getString(COLUMNA_GRU_CON_CODI));
						ResultSet resultInfoGrup = statementInfoGrup.executeQuery();
						while(resultInfoGrup.next()) {
							Grup grup = new Grup();
							grup.setNom(resultInfoGrup.getString(COLUMNA_GRUP_NOM));
							grup.setCodi(resultInfoGrup.getInt(COLUMNA_GRUP_CODI));
							grups.add(grup);		
						}						
					}
				}
				personal.setGrups(grups);
				
				
			}
			
		}

		return personal;

	}

	public Feina getInfoFeina(String nif) throws SQLException {

		Feina feina = null;
		ArrayList<Telefon> telefons = new ArrayList<Telefon>();
		String queryInfoPersonal = "SELECT * FROM " + TABLE_FEINA + " WHERE " + COLUMNA_NIF + " = ?  ";
		try (PreparedStatement statementInfoPersonals = connection.prepareStatement(queryInfoPersonal)) {
			statementInfoPersonals.setString(1, nif);

			ResultSet resultPersonal = statementInfoPersonals.executeQuery();
			while (resultPersonal.next()) {
				feina = new Feina();
				feina.setNif(resultPersonal.getString(COLUMNA_NIF));
				feina.setAdreca(resultPersonal.getString(COLUMNA_ADRECA));
				feina.setNom(resultPersonal.getString(COLUMNA_NOM));
				feina.setWeb(resultPersonal.getString(COLUMNA_WEB));
			}
			if(feina==null) {
				return null;
			}
			String getTelefons = "SELECT * FROM "+ TABLE_TELEFON + " WHERE "+ COLUMNA_TELEFON_NIF_CONTACTE +"= ?";
			try(PreparedStatement statementTelefons = connection.prepareStatement(getTelefons)){
				statementTelefons.setString(1, nif);
				ResultSet resultats = statementTelefons.executeQuery();
				
				while(resultats.next()) {
					Telefon telefon = new Telefon();
					telefon.setNumero(resultats.getInt(COLUMNA_TELEFON_NUMERO));
					telefons.add(telefon);
				}
				feina.setTelefons(telefons);
				
			}
			String queryGrups = "SELECT * FROM "+TABLE_GRU_CON +" WHERE "+ COLUMNA_GRU_CON_NIF +" = ? ";
			try(PreparedStatement statementGrups = connection.prepareStatement(queryGrups)){
				statementGrups.setString(1, nif);
				ResultSet resultatsGrups = statementGrups.executeQuery();
				ArrayList<Grup> grups = new ArrayList<Grup>();
				while(resultatsGrups.next()) {
					String queryDadesGrup = "SELECT * FROM "+TABLE_GRUP+" WHERE "+ COLUMNA_GRUP_CODI +" = ?";
					try(PreparedStatement statementInfoGrup = connection.prepareStatement(queryDadesGrup)){
						statementInfoGrup.setString(1, resultatsGrups.getString(COLUMNA_GRU_CON_CODI));
						ResultSet resultInfoGrup = statementInfoGrup.executeQuery();
						while(resultInfoGrup.next()) {
							Grup grup = new Grup();
							grup.setNom(resultInfoGrup.getString(COLUMNA_GRUP_NOM));
							grup.setCodi(resultInfoGrup.getInt(COLUMNA_GRUP_CODI));
							grups.add(grup);		
						}						
					}
				}
				feina.setGrups(grups);
				
				
			}
		}

		return feina;

	}

	@Override
	public Contacte buscarUnicContacte(Contacte contacte) {
		Personal personal = null;
		Feina contacteTrobat = null;
		try {
			openConnection();

			contacteTrobat = getInfoFeina(contacte.getNif());
			if (contacteTrobat == null) {
				 personal = getInfoPersonal(contacte.getNif());
			}
			closeConnection();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		if (contacteTrobat == null) {
			return personal;
		}
		return contacteTrobat;
	}

	@Override
	public void afegirTelefonAContacte(Contacte contacte, Telefon telefon) {
		try {
			openConnection();
			
			String query = "INSERT INTO "+ TABLE_TELEFON+" VALUES(?,?)";
			try(PreparedStatement statemet = connection.prepareStatement(query)){
				statemet.setInt(1, telefon.getNumero());
				statemet.setString(2, contacte.getNif());
				statemet.executeUpdate();
			}
	
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	public List<Grup> infoGrups (){
		
	ArrayList<Grup> grups = new ArrayList<Grup>();
	try {
		openConnection();
		String queryGrups = "SELECT * FROM "+TABLE_GRUP;
		try(PreparedStatement statementGrups = connection.prepareStatement(queryGrups)){
			
			ResultSet resultatsGrups = statementGrups.executeQuery();
			while(resultatsGrups.next()) {
				
				
						Grup grup = new Grup();
						grup.setNom(resultatsGrups.getString(COLUMNA_GRUP_NOM));
						grup.setCodi(resultatsGrups.getInt(COLUMNA_GRUP_CODI));
						grups.add(grup);		
											
				}
			}
		
	
	} catch (SQLException e) {
		e.printStackTrace();
	}

		return grups;
	
	
	
	
	}
}
