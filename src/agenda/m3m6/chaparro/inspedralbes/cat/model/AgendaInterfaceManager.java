package agenda.m3m6.chaparro.inspedralbes.cat.model;

import java.util.ArrayList;
import java.util.List;

import agenda.m3m6.chaparro.inspedralbes.cat.entities.Contacte;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Telefon;

public interface AgendaInterfaceManager {

	public void createDataBaseWithSamples();
	public void afegirContacte(Contacte contacte);
	public void eliminarContacte(Contacte contacte);
	public void afegirTelefonAContacte(Contacte contacte, Telefon telefon);
	public void actualitzarContacte(Contacte contacteOriginal, Contacte contacteActualitzat);
	public List<Contacte> buscarTotContactes();
	public List<Contacte> buscarContactesPerNom(String nom);
	//public void veureTotsUnaCerca
	//public void veureDadesContacteIGrups(Contacte contacte);
	public void crearGrup(Grup grup);
	public void eliminarGrup(Grup grup);
	public void afegirContacteAGrup(Contacte contacte, Grup grup);
	public void eliminarContacteDeGrup(Contacte contacte, Grup grup);
	public Grup veureDadesGrup(Grup grup);
	public Contacte buscarUnicContacte(Contacte contacte);
	public List<Grup> infoGrups();	
	
}
