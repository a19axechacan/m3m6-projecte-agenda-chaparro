package agenda.m3m6.chaparro.inspedralbes.cat.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import agenda.m3m6.chaparro.inspedralbes.cat.entities.Contacte;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Telefon;

public class AgendaHibernateManager implements AgendaInterfaceManager {

	EntityManagerFactory emf;
	EntityManager em;

	public void openConnection() {
		emf = Persistence.createEntityManagerFactory("M3M6-Projecte-Agenda-Chaparro");
		em = emf.createEntityManager();
	}

	public void closeConnection() {

		em.close();
		emf.close();
	}

	@Override
	public void afegirContacte(Contacte contacte) {

		openConnection();
		em.getTransaction().begin();
		em.persist(contacte);
		em.getTransaction().commit();
		closeConnection();

	}

	@Override
	public void eliminarContacte(Contacte contacte) {	
		
		openConnection();
		em.getTransaction().begin();
		Contacte contacte1 = em.find(Contacte.class, contacte.getNif());
		em.remove(contacte1);
		em.getTransaction().commit();
		closeConnection();
		
	}

	@Override
	public void actualitzarContacte(Contacte contacteOriginal, Contacte contacteActualitzat) {
		openConnection();
		em.getTransaction().begin();
		Contacte contactePrimer = em.find(Contacte.class, contacteOriginal.getNif());

		contactePrimer.setNom(contacteActualitzat.getNom());
		contactePrimer.setAdreca(contacteActualitzat.getAdreca());

		em.getTransaction().commit();
		closeConnection();
	}

	@Override
	public List<Contacte> buscarTotContactes() {
		openConnection();
		TypedQuery<Contacte> query = em.createQuery("SELECT c FROM Contacte c", Contacte.class);
		ArrayList<Contacte> resultat = (ArrayList<Contacte>) query.getResultList();
		closeConnection();
		return resultat;
	}

	@Override
	public List<Contacte> buscarContactesPerNom(String nom) {

		openConnection();
		TypedQuery<Contacte> query = em.createQuery("SELECT c FROM Contacte c WHERE c.nom= '" + nom + "'",
				Contacte.class);
		ArrayList<Contacte> resultat = (ArrayList<Contacte>) query.getResultList();
		closeConnection();
		return resultat;
	}

	@Override
	public void crearGrup(Grup grup) {
		// TODO
		// TODOS TIENEN EL CODIGO 0
		openConnection();
		em.getTransaction().begin();
		em.persist(grup);
		em.getTransaction().commit();
		closeConnection();
	}

	@Override
	public void eliminarGrup(Grup grup) {
		//TODO
		//EL GRUPO NO SE ELIMINA SI HAY ALGUIEN 
		openConnection();
		em.getTransaction().begin();
		Grup grupDelete = em.merge(grup);
		em.remove(grupDelete);
		em.getTransaction().commit();
		closeConnection();

	}

	@Override
	public void afegirContacteAGrup(Contacte contacte, Grup grup) {
		openConnection();
		em.getTransaction().begin();
		Contacte afegir = em.find(Contacte.class, contacte.getNif());
		Grup grupAfegir = em.find(Grup.class, grup.getCodi());
		afegir.getGrups().add(grupAfegir);
		em.persist(afegir);
		em.getTransaction().commit();
		closeConnection();
	}

	@Override
	public void eliminarContacteDeGrup(Contacte contacte, Grup grup) {
		openConnection();
		em.getTransaction().begin();
		Contacte treure = em.find(Contacte.class, contacte.getNif());
		Grup grupATreure = em.find(Grup.class, grup.getCodi());
		treure.getGrups().remove(grupATreure);
		em.persist(treure);
		em.getTransaction().commit();
		closeConnection();

	}

	@Override
	public Grup veureDadesGrup(Grup grup) {

		openConnection();
		TypedQuery<Grup> query = em.createQuery("SELECT g FROM Grup g WHERE g.codi= '" + grup.getCodi() + "'",
				Grup.class);
		Grup grupTrobat = query.getSingleResult();
		closeConnection();
		return grupTrobat;
	}

	@Override
	public Contacte buscarUnicContacte(Contacte contacte) {

		openConnection();

		TypedQuery<Contacte> query = em.createQuery("SELECT c FROM Contacte c WHERE c.nif= '" + contacte.getNif() + "'",
				Contacte.class);
		Contacte contacteTrobat = query.getSingleResult();

		closeConnection();
		return contacteTrobat;
	}



	@Override
	public List<Grup> infoGrups() {

		openConnection();
		
		
		Query query = em.createQuery("SELECT g FROM Grup g");
		
		ArrayList<Grup> grups = (ArrayList<Grup>) query.getResultList();
		
		
		closeConnection();
		
		return grups;
	}

	@Override
	public void afegirTelefonAContacte(Contacte contacte, Telefon telefon) {
		openConnection();
		em.getTransaction().begin();
		Contacte afegir = em.find(Contacte.class, contacte.getNif());
		afegir.getTelefons().add(telefon);
		em.persist(afegir);
		em.getTransaction().commit();
		closeConnection();
		
	}

	@Override
	public void createDataBaseWithSamples() {

	}

}
