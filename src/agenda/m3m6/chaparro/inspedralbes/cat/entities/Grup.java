package agenda.m3m6.chaparro.inspedralbes.cat.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.List;


/**
 * The persistent class for the GRUP database table.
 * 
 */
@Entity
@Table(name="GRUP")
@NamedQuery(name="Grup.findAll", query="SELECT g FROM Grup g")
public class Grup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int codi;

	private String nom;

	//bi-directional many-to-many association to Contacte
	@ManyToMany(mappedBy = "grups")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Contacte> contactes;

	public Grup() {
	}

	public int getCodi() {
		return this.codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Contacte> getContactes() {
		return this.contactes;
	}

	public void setContactes(List<Contacte> contactes) {
		this.contactes = contactes;
	}

	public String infoGrup() {
		return "Nom: "+nom+", Codi: "+codi;
	}
	
	

}