package agenda.m3m6.chaparro.inspedralbes.cat.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FEINA database table.
 * 
 */
@Entity
@Table(name="FEINA")
@NamedQuery(name="Feina.findAll", query="SELECT f FROM Feina f")
public class Feina extends Contacte implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private String web;

	public Feina() {
	}


	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

}