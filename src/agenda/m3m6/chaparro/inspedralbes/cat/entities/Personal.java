package agenda.m3m6.chaparro.inspedralbes.cat.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PERSONAL database table.
 * 
 */
@Entity
@Table(name="PERSONAL")
@NamedQuery(name="Personal.findAll", query="SELECT p FROM Personal p")
public class Personal extends Contacte implements Serializable {
	private static final long serialVersionUID = 1L;



	private String imatge;



	public Personal() {
	}

	



	public String getImatge() {
		return this.imatge;
	}

	public void setImatge(String imatge) {
		this.imatge = imatge;
	}


	

}