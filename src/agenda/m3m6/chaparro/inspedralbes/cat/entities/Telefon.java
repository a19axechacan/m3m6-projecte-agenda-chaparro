package agenda.m3m6.chaparro.inspedralbes.cat.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TELEFON database table.
 * 
 */
@Entity
@Table(name="TELEFON")
@NamedQuery(name="Telefon.findAll", query="SELECT t FROM Telefon t")
public class Telefon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int numero;

	
	@ManyToOne
	@JoinColumn(name ="id_contacte" )
	private Contacte contacte;

	public Telefon() {
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Contacte getContacte() {
		return this.contacte;
	}

	public void setContacte(Contacte contacte) {
		this.contacte = contacte;
	}

	@Override
	public String toString() {
		return "Telefon [numero=" + numero + "]";
	}
	
	
	

}