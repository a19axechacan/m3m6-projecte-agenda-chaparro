package agenda.m3m6.chaparro.inspedralbes.cat.entities;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.List;


/**
 * The persistent class for the CONTACTE database table.
 * 
 */

@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
@Table (name= "CONTACTE")
public class Contacte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String nif;

	private String adreca;

	private String nom;

	//bi-directional many-to-many association to Grup
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(
			name="GRU_CON"
			, joinColumns={
				@JoinColumn(name="nif")
				}
			, inverseJoinColumns={
				@JoinColumn(name="codi")
				}
			)
	private List<Grup> grups;

	
	@OneToMany(mappedBy="contacte",cascade = CascadeType.ALL, fetch = FetchType.EAGER )
	private List<Telefon> telefons;

	

	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getAdreca() {
		return this.adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Grup> getGrups() {
		return this.grups;
	}

	public void setGrups(List<Grup> grups) {
		this.grups = grups;
	}

	public List<Telefon> getTelefons() {
		return this.telefons;
	}

	public void setTelefons(List<Telefon> telefons) {
		this.telefons = telefons;
	}

	public Telefon addTelefon(Telefon telefon) {
		getTelefons().add(telefon);
		telefon.setContacte(this);

		return telefon;
	}

	public Telefon removeTelefon(Telefon telefon) {
		getTelefons().remove(telefon);
		telefon.setContacte(null);

		return telefon;
	}

	@Override
	public String toString() {
		return "Contacte [nif=" + nif + ", adreca=" + adreca + ", nom=" + nom + ", grups=" + grups + ", telefons="
				+ telefons + "]";
	}
	
	
	

}