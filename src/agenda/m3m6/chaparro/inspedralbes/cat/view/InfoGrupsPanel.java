package agenda.m3m6.chaparro.inspedralbes.cat.view;

import javax.swing.JPanel;

import agenda.m3m6.chaparro.inspedralbes.cat.entities.Contacte;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Feina;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Personal;

import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.JLabel;

public class InfoGrupsPanel extends JPanel {
	private JLabel lblCodiGrup;
	private JLabel lblNomGrup;

	/**
	 * Create the panel.
	 */
	public InfoGrupsPanel(Grup grup) {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		
		 lblNomGrup = new JLabel("");
		panel.add(lblNomGrup);
		
		lblCodiGrup = new JLabel("");
		panel.add(lblCodiGrup);
		
		JPanel panelContactes = new JPanel();

		JLabel labelTitle = new JLabel("Contactes:");
		
		for (Contacte contacte : grup.getContactes()) {
			
			
			JLabel label = new JLabel();
		
			String infoContacte = contacte.getNif()+"-"+contacte.getNom()+"-"+contacte.getAdreca()+"-";
			
			if(contacte instanceof Feina) {
				
				infoContacte+=((Feina) contacte).getWeb();
				
			}else if(contacte instanceof Personal) {
				infoContacte+=((Personal) contacte).getImatge();
			}
			
			
			label.setText(infoContacte);
			
			panelContactes.add(label);
			
		}
		
		
		add(panelContactes, BorderLayout.CENTER);
		panelContactes.setLayout(new BoxLayout(panelContactes, BoxLayout.Y_AXIS));

	}

	public String getLblNomGrupText() {
		return lblNomGrup.getText();
	}
	public void setLblNomGrupText(String text) {
		lblNomGrup.setText("Nom: "+text+"        ");
	}
	public String getLblCodiGrupText() {
		return lblCodiGrup.getText();
	}
	public void setLblCodiGrupText(String text_1) {
		lblCodiGrup.setText("Codi: "+text_1+"        ");
	}
}
