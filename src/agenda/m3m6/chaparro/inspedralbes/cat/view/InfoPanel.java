package agenda.m3m6.chaparro.inspedralbes.cat.view;

import javax.swing.JPanel;

import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Telefon;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.Box;

public class InfoPanel extends JPanel {
	private JLabel lblNom;
	private JLabel lblAdreca;
	private JLabel lblExtra;
	private JPanel panelTelefons;
	private JPanel panelGrups;
	private JLabel lblTipus;
	private JLabel lblNif;
	private Component horizontalStrut;

	/**
	 * Create the panel.
	 */
	public InfoPanel(List<Grup> grups, List<Telefon> telefons) {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelCenter = new JPanel();
		add(panelCenter, BorderLayout.CENTER);
		panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.X_AXIS));
		
		panelTelefons = new JPanel();
		panelTelefons.setLayout(new BoxLayout(panelTelefons, BoxLayout.Y_AXIS));
		
		JLabel labelTel = new JLabel("Telefons: ");
		panelTelefons.add(labelTel);
		for(Telefon telefon : telefons) {
			
			JLabel label = new JLabel(telefon.getNumero()+"");
			panelTelefons.add(label);
			
		}
		
		panelCenter.add(panelTelefons);
		
		
		panelGrups = new JPanel();
		panelGrups.setLayout(new BoxLayout(panelGrups, BoxLayout.Y_AXIS));
		
		JLabel labelGrup = new JLabel("Grups: ");
		panelGrups.add(labelGrup);
		for(Grup grup : grups) {
			
			JLabel label = new JLabel(grup.getNom());
			panelGrups.add(label);
			
		}
		
		horizontalStrut = Box.createHorizontalStrut(250);
		panelCenter.add(horizontalStrut);
		panelCenter.add(panelGrups);
		
		
		JPanel panelNorth = new JPanel();
		add(panelNorth, BorderLayout.NORTH);
		panelNorth.setLayout(new GridLayout(0, 5, 0, 0));
		
		lblNom = new JLabel("");
		panelNorth.add(lblNom);
		
		lblNif = new JLabel("");
		panelNorth.add(lblNif);
		
		lblAdreca = new JLabel("");
		panelNorth.add(lblAdreca);
		
		lblExtra = new JLabel("");
		panelNorth.add(lblExtra);
		
		lblTipus = new JLabel("");
		panelNorth.add(lblTipus);

	}

	public String getLblNomText() {
		return lblNom.getText();
	}
	public void setLblNomText(String text) {
		lblNom.setText(text);
	}
	public String getLblAdrecaText() {
		return lblAdreca.getText();
	}
	public void setLblAdrecaText(String text_1) {
		lblAdreca.setText(text_1);
	}
	public String getLblExtraText() {
		return lblExtra.getText();
	}
	public void setLblExtraText(String text_2) {
		lblExtra.setText(text_2);
	}
	public String getLblTipusText() {
		return lblTipus.getText();
	}
	public void setLblTipusText(String text_3) {
		lblTipus.setText(text_3);
	}
	public String getLblNifText() {
		return lblNif.getText();
	}
	public void setLblNifText(String text_4) {
		lblNif.setText(text_4);
	}
}
