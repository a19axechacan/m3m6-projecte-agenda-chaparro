package agenda.m3m6.chaparro.inspedralbes.cat.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import agenda.m3m6.chaparro.inspedralbes.cat.entities.Contacte;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Feina;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Personal;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Telefon;
import agenda.m3m6.chaparro.inspedralbes.cat.model.AgendaHSQLDBManager;
import agenda.m3m6.chaparro.inspedralbes.cat.model.AgendaHibernateManager;
import agenda.m3m6.chaparro.inspedralbes.cat.model.AgendaInterfaceManager;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;

import java.awt.Dimension;
import java.awt.Component;
import javax.swing.Box;

public class AgendaView extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JMenuItem mntmNouContacte;
	public AgendaInterfaceManager manager ;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPanel cards;
	private JTextField textFieldGuardarNom;
	private JTextField textFieldGuardarNif;
	private JTextField textFieldGuardarAdreca;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField textFieldGuardarImatge;
	private JTextField textFieldGuardarWeb;
	private JRadioButton rdbtnPersonal;
	private JRadioButton rdbtnFeina;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JTextField textFieldActualitzar;
	private JTextField textFieldActualitzarImatge;
	private JTextField textFieldActualitzarWeb;
	private JTextField textFieldActualitzarNom;
	private JTextField textFieldActualitzarAdreca;
	private JRadioButton rdbtnPersonalActualitzar;
	private JRadioButton rdbtnFeinaActualitzar;
	private JPanel panelInfoContacte;

	private JMenuItem mntmVeureTots;
	private JPanel panelInfoGrup;
	private JLabel lblCodiGrup;
	private JPanel panelInfoNoms;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgendaView frame = new AgendaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AgendaView() {
		
		
		String [] arrayBDD = {"HSQLDB", "Hibernate-MySQL"};
		
		int option = JOptionPane.showOptionDialog(this, "Selecciona una base de dades", "Selecci� BDD",JOptionPane.INFORMATION_MESSAGE, JOptionPane.CANCEL_OPTION, null, arrayBDD, arrayBDD[0]);
		
		if(option==0) {
			manager = new AgendaHSQLDBManager();
		}else manager = new AgendaHibernateManager();
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1050, 310);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu menuContactes = new JMenu("Contactes");
		menuBar.add(menuContactes);

		mntmNouContacte = new JMenuItem("Nou");
		mntmNouContacte.setActionCommand("nouContacteMenu");
		mntmNouContacte.addActionListener(this);
		menuContactes.add(mntmNouContacte);

		mntmVeureTots = new JMenuItem("Veure Tots");
		mntmVeureTots.setActionCommand("veureTotsMenu");
		mntmVeureTots.addActionListener(this);
		menuContactes.add(mntmVeureTots);

		JMenuItem mntmBuscarNom = new JMenuItem("Buscar per nom");
		mntmBuscarNom.setActionCommand("buscarPerNomMenu");
		mntmBuscarNom.addActionListener(this);
		menuContactes.add(mntmBuscarNom);

		JMenu mnNewMenu_1 = new JMenu("Grups");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Nou Grup");
		mntmNewMenuItem_2.setActionCommand("nouGrup");
		mntmNewMenuItem_2.addActionListener(this);
		mnNewMenu_1.add(mntmNewMenuItem_2);

		JMenuItem mntmVeureGrups = new JMenuItem("Veure tots els grups");
		mntmVeureGrups.setActionCommand("veureGrupsMenu");
		mntmVeureGrups.addActionListener(this);
		mnNewMenu_1.add(mntmVeureGrups);

		JMenu mnNewMenu = new JMenu("Base de dades");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Reinicar-Crear");
		mntmNewMenuItem.addActionListener(this);
		mntmNewMenuItem.setActionCommand("ResetBD");
		mnNewMenu.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		cards = new JPanel();
		contentPane.add(cards, BorderLayout.CENTER);
		cards.setLayout(new CardLayout(0, 0));

		JPanel panelNouContacte = new JPanel();
		cards.add(panelNouContacte, "nouContacte");
		panelNouContacte.setLayout(new BorderLayout(0, 0));

		JLabel lblNouContacte = new JLabel("Nou Contacte");
		panelNouContacte.add(lblNouContacte, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		panelNouContacte.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		textFieldGuardarNom = new JTextField();
		textFieldGuardarNom.setBounds(39, 43, 114, 19);
		panel.add(textFieldGuardarNom);
		textFieldGuardarNom.setColumns(10);

		JLabel lblNewLabel = new JLabel("Nom");
		lblNewLabel.setBounds(39, 25, 70, 15);
		panel.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Nif");
		lblNewLabel_1.setBounds(39, 74, 70, 15);
		panel.add(lblNewLabel_1);

		textFieldGuardarNif = new JTextField();
		textFieldGuardarNif.setBounds(39, 101, 114, 19);
		panel.add(textFieldGuardarNif);
		textFieldGuardarNif.setColumns(10);

		JLabel lblAdrea = new JLabel("Adreça");
		lblAdrea.setBounds(39, 132, 70, 15);
		panel.add(lblAdrea);

		textFieldGuardarAdreca = new JTextField();
		textFieldGuardarAdreca.setBounds(39, 159, 114, 19);
		panel.add(textFieldGuardarAdreca);
		textFieldGuardarAdreca.setColumns(10);

		textFieldGuardarImatge = new JTextField();
		textFieldGuardarImatge.setBounds(165, 101, 114, 19);
		panel.add(textFieldGuardarImatge);
		textFieldGuardarImatge.setColumns(10);

		textFieldGuardarWeb = new JTextField();
		textFieldGuardarWeb.setColumns(10);
		textFieldGuardarWeb.setBounds(165, 159, 114, 19);
		panel.add(textFieldGuardarWeb);

		rdbtnPersonal = new JRadioButton("Personal-imatge");
		rdbtnPersonal.setSelected(true);

		buttonGroup_1.add(rdbtnPersonal);
		rdbtnPersonal.setBounds(165, 70, 149, 23);
		panel.add(rdbtnPersonal);

		JLabel lblNewLabel_2 = new JLabel("Tipus");
		lblNewLabel_2.setBounds(165, 25, 70, 15);
		panel.add(lblNewLabel_2);

		rdbtnFeina = new JRadioButton("Feina - web");

		buttonGroup_1.add(rdbtnFeina);
		rdbtnFeina.setBounds(165, 128, 149, 23);
		panel.add(rdbtnFeina);

		JButton btnGuardarContacte = new JButton("Guardar");
		btnGuardarContacte.setActionCommand("guardarContacte");
		btnGuardarContacte.addActionListener(this);
		btnGuardarContacte.setBounds(311, 190, 117, 25);
		panel.add(btnGuardarContacte);

		JPanel panelActualitzarContacte = new JPanel();
		cards.add(panelActualitzarContacte, "actualitzarContacte");
		panelActualitzarContacte.setLayout(new BorderLayout(0, 0));

		JLabel lblActualitzarContacte = new JLabel("Actualitzar Contacte");
		panelActualitzarContacte.add(lblActualitzarContacte, BorderLayout.NORTH);

		JPanel textFieldActualitzarNif = new JPanel();
		panelActualitzarContacte.add(textFieldActualitzarNif, BorderLayout.CENTER);
		textFieldActualitzarNif.setLayout(null);

		rdbtnPersonalActualitzar = new JRadioButton("Personal - imatge");
		rdbtnPersonalActualitzar.setSelected(true);
		buttonGroup_2.add(rdbtnPersonalActualitzar);
		rdbtnPersonalActualitzar.setBounds(258, 38, 150, 23);
		textFieldActualitzarNif.add(rdbtnPersonalActualitzar);

		rdbtnFeinaActualitzar = new JRadioButton("Feina-web");
		buttonGroup_2.add(rdbtnFeinaActualitzar);
		rdbtnFeinaActualitzar.setBounds(258, 98, 150, 23);
		textFieldActualitzarNif.add(rdbtnFeinaActualitzar);

		JLabel lblTipues = new JLabel("Tipus");
		lblTipues.setBounds(258, 22, 70, 15);
		textFieldActualitzarNif.add(lblTipues);

		JLabel lblNif_1 = new JLabel("Nif original");
		lblNif_1.setBounds(12, 22, 98, 15);
		textFieldActualitzarNif.add(lblNif_1);

		textFieldActualitzar = new JTextField();
		textFieldActualitzar.setBounds(12, 69, 114, 19);
		textFieldActualitzarNif.add(textFieldActualitzar);
		textFieldActualitzar.setColumns(10);

		textFieldActualitzarImatge = new JTextField();
		textFieldActualitzarImatge.setBounds(258, 69, 114, 19);
		textFieldActualitzarNif.add(textFieldActualitzarImatge);
		textFieldActualitzarImatge.setColumns(10);

		textFieldActualitzarWeb = new JTextField();
		textFieldActualitzarWeb.setBounds(258, 129, 114, 19);
		textFieldActualitzarNif.add(textFieldActualitzarWeb);
		textFieldActualitzarWeb.setColumns(10);

		JLabel lblNom = new JLabel("Nou nom");
		lblNom.setBounds(12, 102, 70, 15);
		textFieldActualitzarNif.add(lblNom);

		textFieldActualitzarNom = new JTextField();
		textFieldActualitzarNom.setBounds(12, 129, 114, 19);
		textFieldActualitzarNif.add(textFieldActualitzarNom);
		textFieldActualitzarNom.setColumns(10);

		JLabel lblAdrea_1 = new JLabel("Nova adreça");
		lblAdrea_1.setBounds(12, 161, 114, 15);
		textFieldActualitzarNif.add(lblAdrea_1);

		textFieldActualitzarAdreca = new JTextField();
		textFieldActualitzarAdreca.setBounds(12, 188, 114, 19);
		textFieldActualitzarNif.add(textFieldActualitzarAdreca);
		textFieldActualitzarAdreca.setColumns(10);

		JButton btnActualitzarContacte = new JButton("Actualitzar");
		btnActualitzarContacte.setBounds(299, 185, 117, 25);
		btnActualitzarContacte.addActionListener(this);
		btnActualitzarContacte.setActionCommand("actualitzarContacte");
		textFieldActualitzarNif.add(btnActualitzarContacte);

		JPanel panelVeureTots = new JPanel();
		cards.add(panelVeureTots, "veureTots");
		panelVeureTots.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel_5 = new JLabel("Contactes");
		panelVeureTots.add(lblNewLabel_5, BorderLayout.NORTH);

		JScrollPane panel_3 = new JScrollPane();
		panelVeureTots.add(panel_3, BorderLayout.CENTER);

		panelInfoContacte = new JPanel();
		panel_3.setViewportView(panelInfoContacte);
		panelInfoContacte.setLayout(new BoxLayout(panelInfoContacte, BoxLayout.Y_AXIS));

		JPanel panel_4 = new JPanel();
		panelInfoContacte.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.X_AXIS));

		JPanel panelBuscarPerNom = new JPanel();
		cards.add(panelBuscarPerNom, "buscarPerNom");
		panelBuscarPerNom.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel_4 = new JLabel("Contactes per nom");
		panelBuscarPerNom.add(lblNewLabel_4, BorderLayout.NORTH);

		JScrollPane scrollPane_1 = new JScrollPane();
		panelBuscarPerNom.add(scrollPane_1, BorderLayout.CENTER);

		panelInfoNoms = new JPanel();
		scrollPane_1.setViewportView(panelInfoNoms);
		panelInfoNoms.setLayout(new BoxLayout(panelInfoNoms, BoxLayout.Y_AXIS));
		
	
		

		JPanel panelGrups = new JPanel();
		cards.add(panelGrups, "veureGrups");
		panelGrups.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel_3 = new JLabel("Grups");
		panelGrups.add(lblNewLabel_3, BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane();
		panelGrups.add(scrollPane, BorderLayout.CENTER);

		panelInfoGrup = new JPanel();
		scrollPane.setViewportView(panelInfoGrup);
		panelInfoGrup.setLayout(new BoxLayout(panelInfoGrup, BoxLayout.Y_AXIS));

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		CardLayout cardLayout = (CardLayout) cards.getLayout();

		switch (e.getActionCommand()) {
		case "nouContacteMenu":
			cardLayout.show(cards, "nouContacte");
			break;

		case "actualitzarContacteMenu":
			cardLayout.show(cards, "actualitzarContacte");
			break;
			
			
		case "ResetBD":
			manager.createDataBaseWithSamples();
			
			break;

		case "veureTotsMenu":
			cardLayout.show(cards, "veureTots");

			ArrayList<Contacte> contactes = (ArrayList<Contacte>) manager.buscarTotContactes();

			panelInfoContacte.removeAll();

			for (Contacte contacte : contactes) {

				Component verticalStrut = Box.createVerticalStrut(20);
				verticalStrut.setMinimumSize(new Dimension(0, 10));
				verticalStrut.setMaximumSize(new Dimension(32767, 10));
				panelInfoContacte.add(verticalStrut);
				JPanel panelContacte = new JPanel();
				panelInfoContacte.add(panelContacte);
				panelContacte.setLayout(new BoxLayout(panelContacte, BoxLayout.X_AXIS));

				JLabel lblNewLabel_10 = new JLabel(contacte.getNif() + "   |");
				panelContacte.add(lblNewLabel_10);

				JLabel lblNewLabel_11 = new JLabel(contacte.getNom() + "   |");
				panelContacte.add(lblNewLabel_11);

				JLabel lblNewLabel_12 = new JLabel(contacte.getAdreca() + "   |");
				panelContacte.add(lblNewLabel_12);

				if (contacte instanceof Feina) {
					JLabel lblNewLabel_13 = new JLabel(((Feina) contacte).getWeb() + "   ");
					panelContacte.add(lblNewLabel_13);
				} else if (contacte instanceof Personal) {
					JLabel lblNewLabel_13 = new JLabel(((Personal) contacte).getImatge() + "   ");
					panelContacte.add(lblNewLabel_13);
				}

				JButton btnNewButton_3 = new JButton("M�s info");
				btnNewButton_3.setActionCommand(contacte.getNif());
				btnNewButton_3.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						Contacte contacteTrobat = manager.buscarUnicContacte(contacte);

						JDialog dialog = new JDialog();
						dialog.setBounds(250, 250, 500, 315);

						List<Grup> grups =  contacteTrobat.getGrups();
						List<Telefon> telefons = contacteTrobat.getTelefons();
						InfoPanel panelInfo = new InfoPanel(grups, telefons);

						panelInfo.setLblNomText(contacteTrobat.getNom());
						panelInfo.setLblNifText(contacteTrobat.getNif());
						panelInfo.setLblAdrecaText(contacteTrobat.getAdreca());
						if (contacteTrobat instanceof Feina) {
							panelInfo.setLblExtraText(((Feina) contacteTrobat).getWeb());
							panelInfo.setLblTipusText("Tipus: Feina");
						} else if (contacteTrobat instanceof Personal) {
							panelInfo.setLblExtraText(((Personal) contacteTrobat).getImatge());
							panelInfo.setLblTipusText("Tipus: Personal");
						}

						dialog.setContentPane(panelInfo);
						dialog.setVisible(true);

					}
				});

				panelContacte.add(btnNewButton_3);

				JButton btnNewButton_4 = new JButton("Afegir telefon");
				btnNewButton_4.setActionCommand(contacte.getNif());
				btnNewButton_4.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						String telefonString = JOptionPane.showInputDialog("Numero a afegir:");

						if (telefonString != null) {
							Telefon telefon = new Telefon();
							telefon.setNumero(Integer.parseInt(telefonString));
							telefon.setContacte(contacte);
							manager.afegirTelefonAContacte(contacte, telefon);
						}

					}
				});
				panelContacte.add(btnNewButton_4);

				JButton btnNewButton_5 = new JButton("Afegir a grup");
				btnNewButton_5.setActionCommand(contacte.getNif());
				btnNewButton_5.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						ArrayList<Grup> grups = (ArrayList<Grup>) manager.infoGrups();

						ArrayList<String> grupNomCodi = new ArrayList<String>();

						for (Grup grup : grups) {
							grupNomCodi.add(grup.getNom() + "-" + grup.getCodi());
						}

						Object[] grupsAndCodisArray = grupNomCodi.toArray();
						if(grupsAndCodisArray.length!=0) {
							Object option = JOptionPane.showInputDialog(null, "Selecctiona un grup", "Selecciona un grup",
									JOptionPane.QUESTION_MESSAGE, null, grupsAndCodisArray, grupsAndCodisArray[0]);

							if (option != null) {
								Grup grup = new Grup();

								String[] grupParts = option.toString().split("-");

								grup.setNom(grupParts[0]);
								grup.setCodi(Integer.parseInt(grupParts[1]));

								manager.afegirContacteAGrup(contacte, grup);

							}
						}else JOptionPane.showMessageDialog(null, "No hi ha grups");
						

					}
				});
				panelContacte.add(btnNewButton_5);

				JButton btnNewButton_8 = new JButton("Treure de grup");
				btnNewButton_8.setActionCommand(contacte.getNif());
				btnNewButton_8.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						ArrayList<Grup> grups = (ArrayList<Grup>) manager.infoGrups();

						ArrayList<String> grupNomCodi = new ArrayList<String>();

						for (Grup grup : grups) {
							grupNomCodi.add(grup.getNom() + "-" + grup.getCodi());
						}

						Object[] grupsAndCodisArray = grupNomCodi.toArray();
						Object option = JOptionPane.showInputDialog(null, "Selecctiona un grup", "Selecciona un grup",
								JOptionPane.QUESTION_MESSAGE, null, grupsAndCodisArray, grupsAndCodisArray[0]);

						if (option != null) {
							Grup grup = new Grup();

							String[] grupParts = option.toString().split("-");

							grup.setNom(grupParts[0]);
							grup.setCodi(Integer.parseInt(grupParts[1]));

							manager.eliminarContacteDeGrup(contacte, grup);

						}

					}
				});
				panelContacte.add(btnNewButton_8);

				JButton btnNewButton_6 = new JButton("Actualitzar");
				btnNewButton_6.setActionCommand(contacte.getNif());
				btnNewButton_6.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						cardLayout.show(cards, "actualitzarContacte");
						textFieldActualitzar.setText(contacte.getNif());
						textFieldActualitzar.setEditable(false);
						textFieldActualitzarAdreca.setText(contacte.getAdreca());
						textFieldActualitzarNom.setText(contacte.getNom());

						if (contacte instanceof Feina) {
							textFieldActualitzarWeb.setText(((Feina) contacte).getWeb());
							textFieldActualitzarImatge.setText("");
							rdbtnFeinaActualitzar.setSelected(true);
							rdbtnPersonalActualitzar.setSelected(false);
						} else if (contacte instanceof Personal) {
							textFieldActualitzarImatge.setText(((Personal) contacte).getImatge());
							textFieldActualitzarWeb.setText("");
							rdbtnPersonalActualitzar.setSelected(true);
							rdbtnFeinaActualitzar.setSelected(false);

						}

					}
				});
				panelContacte.add(btnNewButton_6);

				JButton btnNewButton_7 = new JButton("Eliminar");
				btnNewButton_7.setActionCommand(contacte.getNif());
				btnNewButton_7.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						Contacte contacteTmp = new Contacte();

						contacteTmp.setNif(e.getActionCommand());

						Contacte contacteTrobat = manager.buscarUnicContacte(contacte);

						manager.eliminarContacte(contacteTrobat);

					}
				});
				panelContacte.add(btnNewButton_7);
			}

			break;

		case "buscarPerNomMenu":
		
			
			
			String nomABuscar = JOptionPane.showInputDialog("Numero a afegir:");
			
			if (nomABuscar != null) {
				ArrayList<Contacte> contactesPerNom = (ArrayList<Contacte>) manager.buscarContactesPerNom(nomABuscar);
				
				String string = "";
				for (Contacte contacte2 : contactesPerNom) {
					string = contacte2.getNif() + "-" + contacte2.getNom() + "-" + contacte2.getAdreca();
					System.out.println(string);
					JLabel labelContacte = new JLabel(string);					
					panelInfoNoms.add(labelContacte);
				}

			}
			cardLayout.show(cards, "buscarPerNom");

			break;

		case "guardarContacte": {
			Contacte contacte = null;
			if (rdbtnFeina.isSelected()) {
				contacte = new Feina();
				((Feina) contacte).setWeb(textFieldGuardarWeb.getText());
			} else if (rdbtnPersonal.isSelected()) {
				contacte = new Personal();
				((Personal) contacte).setImatge(textFieldGuardarImatge.getText());
			}

			String nif = textFieldGuardarNif.getText();
			String nom = textFieldGuardarNom.getText();
			String adreca = textFieldGuardarAdreca.getText();

			contacte.setNif(nif);
			contacte.setNom(nom);
			contacte.setAdreca(adreca);

			manager.afegirContacte(contacte);

		}
			break;

		case "actualitzarContacte": {
			Contacte contacteTmp = new Contacte();
			contacteTmp.setNif(textFieldActualitzar.getText());
			Contacte contacteOriginal = manager.buscarUnicContacte(contacteTmp);

			Contacte contacteActualitzat = null;

			if (rdbtnFeinaActualitzar.isSelected()) {
				contacteActualitzat = new Feina();
				((Feina) contacteActualitzat).setWeb(textFieldActualitzarWeb.getText());
			} else if (rdbtnPersonalActualitzar.isSelected()) {
				contacteActualitzat = new Personal();
				((Personal) contacteActualitzat).setImatge(textFieldActualitzarImatge.getText());
			}
			contacteActualitzat.setNom(textFieldActualitzarNom.getText());
			contacteActualitzat.setAdreca(textFieldActualitzarAdreca.getText());

			manager.actualitzarContacte(contacteOriginal, contacteActualitzat);

		}
			break;

		case "veureGrupsMenu":

			cardLayout.show(cards, "veureGrups");

			panelInfoGrup.removeAll();

			ArrayList<Grup> grups = (ArrayList<Grup>) manager.infoGrups();

			for (Grup grup : grups) {

				JPanel panel_1 = new JPanel();
				panelInfoGrup.add(panel_1);
				panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));

				JLabel lblNomGrup = new JLabel(grup.getNom() + "   |        ");
				panel_1.add(lblNomGrup);

				lblCodiGrup = new JLabel(grup.getCodi() + "       ");
				panel_1.add(lblCodiGrup);

				JButton btnInfoGrup = new JButton("M�s info");
				btnInfoGrup.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						Grup grupTrobat = manager.veureDadesGrup(grup);

						JDialog dialog = new JDialog();
						dialog.setBounds(250, 250, 500, 315);

						InfoGrupsPanel infoGrupPanel = new InfoGrupsPanel(grupTrobat);
						infoGrupPanel.setLblCodiGrupText(grupTrobat.getCodi() + "");
						infoGrupPanel.setLblNomGrupText(grupTrobat.getNom());

						dialog.setContentPane(infoGrupPanel);
						dialog.setVisible(true);

					}
				});
				panel_1.add(btnInfoGrup);

				JButton btnEliminarGrup = new JButton("Eliminar");
				btnEliminarGrup.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						manager.eliminarGrup(grup);

					}
				});
				panel_1.add(btnEliminarGrup);

			}

			break;

		case "nouGrup":

			String nomGrup = JOptionPane.showInputDialog("Nom del nou grup:");

			if (nomGrup != null) {

				Grup grupNou = new Grup();
				grupNou.setNom(nomGrup);
				manager.crearGrup(grupNou);
			}

			break;

		default:
			break;
		}

	}

	public String getLblCodiGrupText() {
		return lblCodiGrup.getText();
	}

	public void setLblCodiGrupText(String text) {
		lblCodiGrup.setText(text);
	}
}
