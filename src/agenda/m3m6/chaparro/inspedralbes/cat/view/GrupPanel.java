package agenda.m3m6.chaparro.inspedralbes.cat.view;

import java.util.ArrayList;

import javax.swing.JPanel;
import agenda.m3m6.chaparro.inspedralbes.cat.entities.Grup;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class GrupPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public GrupPanel(ArrayList<Grup> grups) {
		
		JComboBox comboBox = new JComboBox();
		for (Grup grup : grups) {
			System.out.println(grup.getNom()+"-"+grup.getCodi());
			comboBox.addItem(grup.getNom()+"-"+grup.getCodi());			
		}
		add(comboBox);

	}

}
